﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSALinkedList2
{
    public class MyLinkedList<T>
    {
        public MyLinkedList(T value)
        {
            this.Head = new Node<T>(value);
            this.Tail = Head;
        }
        public Node<T> Head { get; private set; }

        //Tail holds the last node in the linkedList but is not implemented yet
        public Node<T> Tail { get; private set; }
        public void AddFirst(Node<T> value)
        {
            if (Head== null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                this.Tail = Head;
                this.Head = value;
                this.Head.Next=Tail;
            }
        }
        public void AddLast(Node<T> value)
        {
            var tmp = Head;
            while (Head.Next!= null)
            {
                Head = Head.Next;
            }
            Head.Next = value;
            Head = tmp;
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            while (Head != null)
            {
                str.AppendLine(Head.Value.ToString());
                Head = Head.Next;
            }
            return str.ToString();
        }
        public void RemoveFirst()
        {
            this.Head = Head.Next;
        }
        public void RemoveLast()
        {
            var tmp = Head;
            var previous = Head;
            while (Head.Next != null)
            {
                previous = Head;
                Head = Head.Next;
            }
            Head = previous;
            Head.Next = null;
            Head = tmp;
        }
        public void RemoveValue(T value)
        {
            var tmp = Head;
            var previous = Head;
            while (Head.Next != null)
            {
                previous = Head;
                if (previous.Next.Value.Equals(value))
                {
                    Head.Next = previous.Next.Next;
                    break;
                }
                Head = Head.Next;
            }
            Head = tmp;
        }





    }
}
