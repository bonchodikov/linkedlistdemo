﻿using System;

namespace DSALinkedList2
{
    public class StartUp
    {
        static void Main()
        {
            int a = 5;
            var ll = new MyLinkedList<int>(a);
            ll.AddLast(new Node<int> (1));
            ll.AddFirst(new Node<int>(10));
            ll.AddFirst(new Node<int>(20));
            ll.AddLast(new Node<int>(40));
            ll.AddFirst(new Node<int>(445));
            ll.AddLast(new Node<int>(4444));
            ll.RemoveValue(40);
            Console.WriteLine(ll.ToString());
        }
    }
}
