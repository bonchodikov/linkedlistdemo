﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedList1.Contracts
{
    public interface INode<T>
    {
        T Value { get; }
        Node<T> Next { get; }
    }
}
