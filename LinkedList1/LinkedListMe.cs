﻿using LinkedList1.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkedList1
{
    public class LinkedListMe<T>:IEnumerable
    {
        public LinkedListMe(Node<T> node)
        {
            this.Head = node;
            this.Tail = node;
            this.Count = 1;
        }
        public Node<T> Head { get; private set; }
        public int Count { get;private set; }
        public Node<T> Tail { get;private set; }

        public void AddAtPostition (int index, Node<T> node)
        {
            //Not implemented yet
            //if (index<0||index>this.Count)
            //{
            //    throw new ArgumentOutOfRangeException("Invalid index!");
            //}
            //var start = Head;
            //var end = new Node<T>(default);
            //int pos = 0;
            //for (int i = 0; i < this.Count; i++)
            //{
            //    if (i==index)
            //    {
                   
            //    }
            //}
        }
        public void AddFront(Node<T> node)
        {
            var tmp = this.Head;
            this.Head = node;
            this.Head.Next = tmp;
            this.Count++;
        }
        public void AddBack(Node<T> node)
        {
            if (Head != null)
            {
                var tmp = this.Tail;
                tmp.Next = node;
                this.Tail = tmp.Next;
            }
            else
            {
                this.Head = node;
            }
            this.Count++;
            this.Tail = node;
        }

        public void RemoveFirst()
        {
            if (this.Count>=2)
            {
                this.Head = Head.Next;
            }
            else
            {
                this.Tail = null;
                this.Head = null;
            }
            this.Count--; 
        }
        public void RemoveLast()
        {
            var tmp = Head;
            if (this.Count >=2)
            {
                while (tmp != null)
                {
                    if (this.Count < 2)
                    {
                        tmp = null;
                        break;
                    }
                    tmp = tmp.Next;
                    if (tmp.Next.Next == null)
                    {
                        break;
                    }
                }
                tmp.Next = null;
            }
            else
            {
                this.Tail = null;
                this.Head = null;
            }
            this.Count--;            
        }


        //public override string ToString()
        //{
        //    var str = new StringBuilder();
        //    while (Head!=null)
        //    {
        //        str.AppendLine(this.Head.Value.ToString());
        //        this.Head = this.Head.Next;
        //    }
        //    return str.ToString();
        //}

        public IEnumerator GetEnumerator()
        {
            Node<T> currentNode = this.Head;
            while (currentNode != null)
            {
                yield return currentNode;
                currentNode = currentNode.Next;
            }
        }
    }
}
