﻿using LinkedList1.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedList1
{
    public class Node<T>:INode<T>
    {
        public Node(T value)
        {
            this.Value = value;
        }
        public T Value { get; set; }
        public Node<T> Next { get; set; }
    }
}
