﻿using System;

namespace LinkedList1
{
    class Program
    {
        static void Main(string[] args)
        {

            var ll = new LinkedListMe<int>(new Node<int>(5));
            ll.AddBack(new Node<int>(6));
            ll.AddFront(new Node<int>(1));            
            ll.AddAfter(1, new Node<int>(7));
           // Console.WriteLine(ll.Count);
           // Console.WriteLine(ll.Tail.Value);

            foreach (Node<int> item in ll)
            {
                Console.WriteLine(item.Value);
            }
            



        }
    }
}
